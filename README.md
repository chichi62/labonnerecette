# Projet django

**Objectif:** Réaliser un site de recettes

### Applications tierces
* Bootstrap pour le design
* Fancybox pour les images des recettes

### Installation des jeux de données
* `git clone https://gitlab.com/chichi62/labonnerecette.git`
* `python manage.py migrate`
* `python manage.py loaddata data.json`
* `python manage.py runserver`


Le jeux de données contient:
* Des types de recettes(entrées, plat, désserts, soupes...)
* Des utilisateurs, recettes, notes et commentaires
* Des photos et des ingredients

### Accés à l'admin
* Login: admin
* Mot de passe: magento62

### Fonctionalité supplémentaire
Envoie par mail la recette à un ami






