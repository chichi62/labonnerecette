from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms.models import inlineformset_factory
from recette.models import Recipe, Pics, Note, Comments, Ingredient

class RegistrationForm(UserCreationForm):
     class Meta:
         model = User
         fields = ['last_name','first_name','username', 'email']

PicFormset = inlineformset_factory(Recipe, Pics, fields=('image', ), can_delete=False, extra=3)
IngredientFormset = inlineformset_factory(Recipe, Ingredient, fields=('name','qty'), can_delete=False, extra=8)


class RecipeForm(forms.ModelForm):
    informationPreparation = forms.CharField(required=True, widget=forms.Textarea(attrs={'cols': 40, 'rows': 2}), label='Informations sur la préparation')
    class Meta:
        model = Recipe
        exclude = []


class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['note']


class CommentForm(forms.ModelForm):
    message = forms.CharField(widget=forms.Textarea(attrs={'cols': 100, 'rows': 5}))
    class Meta:
        model = Comments
        fields = ['message']


class FriendForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'L\'adresse email de mon ami(e)'}))
    message = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Mon message'}))

