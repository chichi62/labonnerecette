from django.contrib import admin
from recette.models import Type, Recipe, Comments, Note, Pics, Ingredient


# Register your models here.
class IngredientAdmin2(admin.ModelAdmin):
    model = Ingredient
    list_display = ('name','qty')
    list_display_links = ('name','qty')

class IngredientAdmin(admin.StackedInline):
    model = Ingredient

class PicsAdmin(admin.StackedInline):
    model = Pics

class RecipeAdmin(admin.ModelAdmin):
    inlines = [IngredientAdmin, PicsAdmin]
    list_display = ('title', 'user')
    list_display_links = ('title', 'user')

class TypeAdmin(admin.ModelAdmin):
    model = Type


class NoteAdmin(admin.ModelAdmin):
    model = Note
    list_display = ('note','recipe','user')
    list_display_links = ('note','recipe','user')

class CommentsAdmin(admin.ModelAdmin):
    model = Comments
    list_display = ('message','recipe','user')
    list_display_links = ('message','recipe','user')


admin.site.register(Recipe, RecipeAdmin)
admin.site.register(Type, TypeAdmin)
admin.site.register(Comments, CommentsAdmin)
admin.site.register(Note, NoteAdmin)
admin.site.register(Ingredient, IngredientAdmin2)