from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Avg
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from recette.forms import RegistrationForm, RecipeForm, PicFormset, FriendForm, IngredientFormset, CommentForm, NoteForm
from recette.models import Recipe, Pics, Type, Ingredient, Comments, Note
from django.core.mail import EmailMultiAlternatives

def index(request):
    # on recupere les dernieres recettes ajouter
    recipes = Recipe.objects.all().order_by('-id');
    typeRecipe = None
    if request.method == 'GET':
        if request.GET.get('type'):
            type = request.GET['type']
            typeRecipe = Type.objects.get(id=type)
            recipes = Recipe.objects.filter(type=type)
    paginator = Paginator(recipes, 10)
    page = request.GET.get('page')
    try:
        recipes = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        recipes = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        recipes = paginator.page(paginator.num_pages)
    contexte = {
        'typeRecipe': typeRecipe,
        'recipes': recipes,
    }
    return render(request, 'recette/index.html', contexte)


def register(request):
    if request.method == 'POST':
        user_form = RegistrationForm(request.POST)
        if user_form.is_valid():
            user = User.objects.create_user(request.POST['username'], request.POST['email'],
                                            request.POST['password1'])
            user.first_name = request.POST['first_name']
            user.last_name = request.POST['last_name']
            user.save()
            context = {
                'form': AuthenticationForm,
                'success_message': 'success'
            }
        return render(request, 'registration/login.html', context)
    else:
        user_form = RegistrationForm()
        context = {
            'form': user_form,
        }
    return render(request, 'registration/register.html', context)


def recipe(request, id):
    if request.method == 'POST':
        form_note = NoteForm(request.POST)
        form_comment = CommentForm(request.POST)

        if form_note.is_valid():
            note = form_note.save()
            note.recipe = Recipe.objects.get(id=id)
            note.user = request.user
            note.save()

        if form_comment.is_valid():
            comment = form_comment.save()
            comment.recipe = Recipe.objects.get(id=id)
            comment.user = request.user
            comment.save()

    recipe = Recipe.objects.get(id=id)
    ingredients = Ingredient.objects.filter(recipe=id)
    pics = Pics.objects.filter(recipe=id)

    comments = Comments.objects.filter(recipe=id)
    form_comment = CommentForm()
    countComments = Comments.objects.filter(recipe=id).count()

    paginator = Paginator(comments, 3)
    page = request.GET.get('page')
    try:
        comments = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        comments = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        comments = paginator.page(paginator.num_pages)

    if (request.user.is_authenticated()):
        noted = Note.objects.filter(recipe=id, user=request.user).count()
        if noted == 0:
            form_note = NoteForm()
        else:
            form_note = ''
    else:
        form_note = ''

    countNotes = Note.objects.filter(recipe=id).count()
    averageNote = Note.objects.filter(recipe=id).aggregate(Avg('note'))

    if pics:
        context = {
           'recipe': recipe,
           'pics': pics,
           'ingredients': ingredients,
           'comments': comments,
           'form_comment': form_comment,
           'form_note': form_note,
           'averageNote': averageNote,
           'countComments': countComments,
           'countNotes' : countNotes
        }
    else:
        context = {
            'recipe': recipe,
            'urlPic': 'recette/img/no-img.jpg',
            'ingredients': ingredients,
            'comments': comments,
            'form_comment': form_comment,
            'form_note': form_note,
            'averageNote': averageNote,
            'countComments': countComments,
            'countNotes': countNotes
        }
    return render(request, 'recette/recipe.html', context)



def add(request):
    if request.method == 'POST':
        recipeForm = RecipeForm(request.POST)
        if recipeForm.is_valid():
            recipe = recipeForm.save()
            recipe.user = request.user
            recipe.save()
            ingredientForm = IngredientFormset(request.POST, instance=recipe)
            if ingredientForm.is_valid():
                ingredientForm.save()
                imgForm = PicFormset(request.POST, request.FILES, instance=recipe)
                if imgForm.is_valid():
                    imgForm.save()
                    return render(request, "recette/add.html", {
                        'recipeForm': recipeForm,
                        'ingredientForm': ingredientForm,
                        'imgForm': imgForm,
                        'success_message': 'success'
                    })

    return render(request, "recette/add.html", {
            "recipeForm": RecipeForm(),
            'ingredientForm': IngredientFormset(),
            "imgForm": PicFormset()
        })


def modify(request, id):
    recipe = get_object_or_404(Recipe, id=id)

    recipeForm = RecipeForm(instance=recipe)
    ingredientForm = IngredientFormset(instance=recipe)
    imgForm = PicFormset(instance=recipe)

    if request.method == 'POST':
        recipeForm = RecipeForm(request.POST)
        if recipeForm.is_valid():
            recipe.title = recipeForm.cleaned_data['title']
            recipe.type = recipeForm.cleaned_data['type']
            recipe.price = recipeForm.cleaned_data['price']
            recipe.difficult = recipeForm.cleaned_data['difficult']
            recipe.preparationTime = recipeForm.cleaned_data['preparationTime']
            recipe.cookingTime = recipeForm.cleaned_data['cookingTime']
            recipe.breakTime = recipeForm.cleaned_data['breakTime']
            recipe.informationPreparation = recipeForm.cleaned_data['informationPreparation']
            recipe.user = request.user
            recipe.save()

            # serializer = serializers.serialize('json', ingredients)
            # return HttpResponse(serializer)


            recipes = Recipe.objects.filter(user_id=request.user.id)
            context = {
                'recipes': recipes,
                'success_message': 'success'
            }
            return render(request, 'recette/profil.html', context)

    return render(request, "recette/modify-recipe.html", {
        'recipe': recipe,
        'ingredientForm': ingredientForm,
        'imgForm': imgForm,
        'recipeForm': recipeForm,
    })


def delete(request, id):
    Recipe.objects.get(id=id).delete()
    recipes = Recipe.objects.filter(user_id=request.user.id)
    context = {
        'recipes': recipes
    }
    return render(request, 'recette/profil.html', context)



def profil(request):
    recipes = None;
    if request.user.is_authenticated():
        recipes = Recipe.objects.filter(user_id=request.user.id)
        context = {
            'recipes': recipes
        }
        return render(request, 'recette/profil.html', context)



# redirection ver profil
def mailAtFriends(request, id):
    if request.user.is_authenticated():
        recipe = Recipe.objects.get(id=id)

        if request.method == 'POST':
            friend_form = FriendForm(request.POST)
            if friend_form.is_valid():
                subject = request.user.first_name + ' vous a envoyé une recette'
                from_email = 'jocelynkelle@gmail.com'
                to = request.POST['email']
                text_content = 'Bonjour,' + request.user.first_name + '(' + request.user.email + ') vous a envoyé une recette.' + request.POST['message'] + '</p><a href="http://127.0.0.1:8000/recipe/' + id +'">Voir la recette de mon amis</a></p>'
                html_content = '<div>Bonjour,<br>' + request.user.first_name + '(' + request.user.email + ') vous a envoyé une recette.<br><br></div><p>' + request.POST["message"] + '</p><a href="http://127.0.0.1:8000/recipe/' + id +'">Voir la recette de mon ami</a></p>'

                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()

                recipes = None;
                recipes = Recipe.objects.filter(user_id=request.user.id)
                context = {
                    'success_send_mail': 'success',
                    'recipes': recipes
                }

                return render(request, 'recette/profil.html', context)
        else:
            friend_form = FriendForm()
            context = {
                'friend_form': friend_form,
                'recipe': recipe,
            }
            return render(request, 'recette/form_friend.html', context)
    else:
        return redirect('login')





def search(request):
    query = request.GET.get('search_query')
    criteria = ''
    orderby = ''
    if request.GET.get('criteria') and request.GET.get('orderby'):
        criteria = request.GET.get('criteria')
        orderby = request.GET.get('orderby')
        if orderby == 'desc':
            results = Recipe.objects.filter(title__contains=query).order_by('-' + criteria).select_related()
        elif orderby == 'asc':
            results = Recipe.objects.filter(title__contains=query).order_by(criteria).select_related()
    else:
        results = Recipe.objects.filter(title__contains=query).select_related()

    paginator = Paginator(results, 1)
    page = request.GET.get('page')
    try:
        results = paginator.page(page)
    except PageNotAnInteger:
        results = paginator.page(1)
    except EmptyPage:
        results = paginator.page(paginator.num_pages)

    context = {
        'page': page,
        'orderby': orderby,
        'criteria': criteria,
        'query': query,
        'results': results
    }

    return render(request, 'recette/result_search.html', context)
