from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

DIFFICULT = (
    ('F', 'Facile'),
    ('M', 'Moyen'),
    ('D', 'Difficile'),
)

NOTE = [(i,i) for i in range(6)]

PRICE = (
    ('1', 'Peu couteux'),
    ('2', 'Couteux'),
    ('3', 'Très couteux'),
)

class Recipe(models.Model):
    user = models.ForeignKey(User, default=1, editable=False)
    title = models.CharField(max_length=100, verbose_name='Titre')
    type = models.ForeignKey('Type', null=True)
    difficult = models.CharField(max_length=1, choices=DIFFICULT, default='F', verbose_name='Difficulté')
    price = models.CharField(max_length=1, choices=PRICE, default='1', verbose_name='Echelle de prix')
    preparationTime = models.IntegerField(null=True, default=20, verbose_name='Temps de préparation')
    cookingTime = models.IntegerField(null=True, default=20, verbose_name='Temps de cuisson')
    breakTime = models.IntegerField(null=True, verbose_name='Temps de repos')
    informationPreparation = models.TextField()


class Type(models.Model):
    title = models.CharField(max_length=256)
    detail = models.TextField()

    def __str__(self):
        return self.title

class Ingredient(models.Model):
    recipe = models.ForeignKey('Recipe', null=True,editable=False)
    name = models.CharField(max_length=100)
    qty = models.CharField(max_length=100)

class Pics(models.Model):
    recipe = models.ForeignKey('Recipe', null=True, editable=False)
    image = models.ImageField(upload_to="picsRecipe", max_length=100)

class Comments(models.Model):
    recipe = models.ForeignKey('Recipe', blank=True, null=True)
    user = models.ForeignKey(User, blank=True, null=True, default=1)
    message = models.TextField()
    created_at = models.DateTimeField(default=datetime.now, blank=True, null=True)

class Note(models.Model):
    recipe = models.ForeignKey('Recipe', blank=True, null=True)
    user = models.ForeignKey(User, blank=True, null=True)
    note = models.IntegerField(choices=NOTE)
