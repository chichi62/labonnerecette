from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^recipe/(?P<id>\d+)/', views.recipe, name="recipe"),
    url(r'^add_recipe', views.add, name="add"),
    url(r'^delete_recipe/(?P<id>\d+)/', views.delete, name='delete'),
    url(r'^modify_recipe/(?P<id>\d+)/', views.modify, name='modify'),
    url(r'^profil/$', views.profil, name='profil'),
    url(r'^search_result/$', views.search, name='search'),
    url(r'^mailAtFriends/(?P<id>\d+)/$', views.mailAtFriends, name='mailAtFriends'),
]