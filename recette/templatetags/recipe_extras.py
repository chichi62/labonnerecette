from django import template
from recette.models import Type, Recipe
import random

register = template.Library()

@register.inclusion_tag('recette/menu.html')
def show_menu():
    return {'types': Type.objects.all()}

@register.simple_tag
def show_count_recipe():
    return Recipe.objects.all().count()

@register.simple_tag
def random_id_recipe():
    sizeRecipe = Recipe.objects.all().count()
    # nombre aleatoire entre 1 et le nombre max de recipe
    return random.randint(1, sizeRecipe)



# @register.inclusion_tag('recette/count_recette_all.html')
# def show_all_recipe():
#     return {'count': Recipe.objects.all().count()}
